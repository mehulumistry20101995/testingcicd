import React, { Component } from 'react';
import Routes from './Routes'
import { BrowserRouter as Router, Route } from "react-router-dom";
import NotFound from './error';

class App extends Component {


  render() {
    return (
     <Router>
      <Route path="/" component={Routes} />
     </Router>
    );
  }
}

export default App;
