import React from 'react'
import Axios from "axios";


const pokeInstance = Axios.create();

export const pokDetails = async (pbmName,id) => {
    const url = `https://pokeapi.co/api/v2/${pbmName}/${id}/`;
    try{
    const httpResponse = await pokeInstance.get(url);
    console.log(httpResponse.data);
    return httpResponse.data;
    }
    catch(e){
      console.log(e);
    }
    return null;
  
  };
  
export const pokOverview = async (name,id) => {

    // Option 1

    // API is broken, uncomment below part when it is fully recover.
    // const url = `https://pokeapi.co/api/v2/${name}/page/${id}`;
    // const httpResponse = await pokeInstance.get(url, {
    //   method: 'GET',
    //   mode: 'no-cors',
    //   headers: {
    //     'Access-Control-Allow-Origin': '*',
    //     'Access-Control-Allow-Headers': "Origin, X-Requested-With, Content-Type, Accept",
    //     'Content-Type': 'application/json',
    //   }
    // });

    // Option 2

    const url = `https://pokeapi.co/api/v2/${name}/`;
    try{
    const httpResponse = await pokeInstance.get(url);
    let arrOfPokeBerrMac = httpResponse.data.results;
    let result = [];
    let jsonResult ={}
    let offset = 20;
    let endValue = (offset*id)+offset;
    let startValue = offset*id;

    if(startValue>arrOfPokeBerrMac.length){
      return null;
    }
    if(endValue>arrOfPokeBerrMac.length){
      endValue = arrOfPokeBerrMac.length;
    }
    for(let i=startValue; i<endValue; i++){
         result.push(arrOfPokeBerrMac[i]);
    }

    jsonResult = {"result":result,"count":arrOfPokeBerrMac.length}
    // perform calculations so that it returns page wise response.
   // console.log(result);   
    return jsonResult 
    }
    catch(e){
      return null;
    }
    
};
