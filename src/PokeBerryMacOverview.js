import React, { Component } from 'react'
import PokeBerryMacOverviewContainer from './PokeBerryMacO';
import { pokOverview } from "./utility/pokemonApi";
import Pagination from 'react-paginate';
import { Route, Switch, Link } from "react-router-dom";
import Axios from "axios";
import "./style.css";
import Overview from './PokeBerryMacOverview';
const pokeInstance = Axios.create();

// Overview Container

export default class PokemonList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: {"result":[],"count":0},
      page: 1,
      namePMB:"",
      pageCount : 1
    };
    this.handlePageChange = this.handlePageChange.bind(this);
  }

  async handlePageChange(page) {

    let tempUrl;
    if(this.state.namePMB === "pokemon"){
      tempUrl = "pokemon"
    }
    else if(this.state.namePMB === "berry"){
      tempUrl = "berries"
    }
    else if(this.state.namePMB === "machine"){
      tempUrl = "machines"
    }
    const url = `/${tempUrl}/page/${page.selected}/`;
    //const httpResponse = await pokeInstance.get(url);
   // console.log(httpResponse);
   this.props.history.push(url);
    let list = await pokOverview(this.state.namePMB,page.selected);
    //list = JSON.stringify(list.result, null, 4);
    console.log(list);
    console.log("page Number", page.selected);
    // in a real app you could query the specific page from a server user list
    this.setState({ list:list,
      page :page.selected});
  }
  componentDidMount = async () => {
    let { match } = this.props;
    const page = match.params.pageId;
    let list;
    let namePMB;
    console.log("Did Mount", match.path);
    if(match.path === "/pokemon/page/:pageId"){  
      namePMB = "pokemon";
       list = await pokOverview("pokemon",page);
    }
    else if(match.path === "/berries/page/:pageId"){
      namePMB = "berry";
      list = await pokOverview("berry",page);
    }
    else if(match.path === "/machines/page/:pageId"){
      namePMB = "machine";
      list = await pokOverview("machine",page);
    }
  
    if(!list){
      throw "Loading";
    }
    let pageCount = list.count;
    //list = JSON.stringify(list.result, null, 4);
    console.log(list);
    this.setState({
      list: list,page:page,namePMB:namePMB,pageCount:pageCount
    });
  };


  render() {

    let { list, page,namePMB,pageCount } = this.state;

    return (
      
      <div> 
      <footer className = "footer">
      <Pagination   
      previousLabel={"Previous"}
      nextLabel={"Next"} 
      breakLabel={<a href="" aria-label="Load More">...</a>}
      breakClassName={"break-me"}
      pageCount={pageCount/20}
      onPageChange={this.handlePageChange}
      disableInitialCallback={ true }
      //intialPage={parseInt(0)}
      forcePage={parseInt(this.state.page)}
      containerClassName={"pagination"}
      subContainerClassName={"pages pagination"}
      activeClassName={"active"} 
      extraAriaContext={"It is Page Number"}/>
      </footer>
      <PokeBerryMacOverviewContainer list = {this.state}/>
     </div>
    );
  }
}
