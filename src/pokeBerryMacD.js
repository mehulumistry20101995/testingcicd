import React, { Component } from "react";
//import { Link } from "react-router-dom";
import { PageHeader} from 'react-bootstrap';
import {Link} from "react-router-dom";
import { BrowserRouter, Route, Switch,Redirect} from "react-router-dom";
import ErrorPage from './error'

class pokeBerryMacD extends Component {
  render() {

  if(this.props.details.details){
      if(this.props.details.pbmName === "pokemon"){
        console.log("hreewe")
        return (
          <div>
          <Link className="p-2 text-dark" to="/">
          Home<br></br>
         </Link><br></br>
         <div>Details of {this.props.details.pbmName}</div><br></br>
           <ul>     
           <li>ID: {this.props.details.details.id}</li>
           <li>Name: {this.props.details.details.name}</li>
           <li>base_experience: {this.props.details.details.base_experience}</li>
           <li>Height: {this.props.details.details.height}</li>
           <li>Weight: {this.props.details.details.weight}</li>
           <li>Moves: {this.props.details.details.moves.map(mov => {
             return(
             <ul>
                <li>{mov.move.name}</li>
             </ul>
             )
           })}</li>
          </ul>
           </div> 
        );
      }
      else if(this.props.details.pbmName === "machines"){
        return (
          <div>
          <Link className="p-2 text-dark" to="/">
           Home<br></br>
          </Link><br></br>
          <div>Details of {this.props.details.pbmName}</div><br></br>
           <ul>     
           <li>ID: {this.props.details.details.id}</li>
           <li>Items: {this.props.details.details.item.name}</li>
           <li>Moves: {this.props.details.details.move.name}</li>
           <li>Version-Group: {this.props.details.details.version_group.name}</li>
          </ul>
           </div> 
        );

      }
      else if(this.props.details.pbmName === "berries"){
        return (
          <div>
          <Link className="p-2 text-dark" to="/">
           Home<br></br><br></br>
          </Link><br></br>
          
          <div>Details of {this.props.details.pbmName}</div><br></br>
           <ul>     
           <li>ID: {this.props.details.details.id}</li>
           <li>Item: {this.props.details.details.item.name}</li>
           <li>Flavors: {this.props.details.details.flavors.map(mov => {
            return(
            <ul>
               <li>{mov.flavor.name}</li>
            </ul>
            )
          })}</li>
           <li>growth_time: {this.props.details.details.growth_time}</li>
           <li>max_harvest: {this.props.details.details.max_harvest}</li>
           <li>size: {this.props.details.details.size}</li>
           <li>smoothness: {this.props.details.details.smoothness}</li>
           <li>soil_dryness: {this.props.details.details.soil_dryness}</li>
          </ul>
           </div> 
        );
      }
      return ( 
        <div>
        <Link className="p-2 text-dark" to="/">
           Home
        </Link><br></br>
        
        </div>
        );
     
    }
    return <Redirect to='/404'  />
  }
}

export default pokeBerryMacD;

//