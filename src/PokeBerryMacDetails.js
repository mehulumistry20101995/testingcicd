import React, { Component } from 'react'
import "./App.css";
import PokeBerryMacDetailsContainer from './pokeBerryMacD';
import { pokDetails } from "./utility/pokemonApi";
import {Link } from "react-router-dom";
// Details Container
export default class PokemonDetails extends Component {


  constructor(props) {
    super(props);

    this.state = {
      details: {},
      pbmName: ""
    };
  }

  componentDidMount = async () => {
    const { match } = this.props;
    const id = match.params.Id;
    let details;
    let pbmName = "";
    console.log("Did Mount", match.path);
    if(match.path === "/pokemon/:Id"){
       pbmName = "pokemon";
       details = await pokDetails("pokemon",id);
    }
    else if(match.path === "/berries/:Id"){
       pbmName = "berries";
       details = await pokDetails("berry",id);
    }
    else if(match.path === "/machines/:Id"){
       pbmName = "machines";
       details = await pokDetails("machine",id);
    }
    
    // if(!details){
    //   throw "page not found";
    // }
   // details = JSON.stringify(details, null, 4);
    this.setState({
      details:details,
      pbmName: pbmName
    });

  };

  render() {
    return(
      <div>
      <PokeBerryMacDetailsContainer details={this.state} />
      </div>
    );
  }
}

