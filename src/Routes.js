import React from 'react'
import { BrowserRouter, Route, Switch } from "react-router-dom";
import HomePage from './HomePage';
import Details from './PokeBerryMacDetails';
import Overview from './PokeBerryMacOverview';
import ErrorPage from './error'

const Router = () => ( 
    <BrowserRouter> 
        <Switch>
            <Route exact path="/" component={HomePage} />
            <Route path="/pokemon/page/:pageId" component={Overview} />
            <Route path="/berries/page/:pageId" component={Overview} />
            <Route path="/machines/page/:pageId" component={Overview} />
            <Route path="/pokemon/:Id" component={Details} />
            <Route path="/berries/:Id" component={Details} />
            <Route path="/machines/:Id" component={Details} />
            <Route path="/:error" component={ErrorPage}/>
        </Switch>
     </BrowserRouter>  
);
export default Router;