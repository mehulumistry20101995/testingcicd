import React, { Component } from 'react'
import { Route, Switch, Link } from "react-router-dom";
import { PageHeader,Button } from 'react-bootstrap';


export default class HomePage extends Component {
  render() {
    return (
      <div>
      <PageHeader>
          Welcome to Pokedex <small>Gotta Catch 'Em All</small>
       </PageHeader>
          <nav className="my-2 my-md-0 mr-md-3">
            <Link className="p-2 text-dark" to="/">
            
              Home
              </Link>
              <p>The Pokédex (ポケモン図鑑 Pokemon Zukan) is an electronic device designed to catalogue and 
              provide information regarding the various species of Pokémon featured in the Pokémon video game, 
              anime and manga series.</p><br></br>
          
            <Link className="p-2 text-dark" to="/berries/page/0">
              Berries
              </Link>
              <p>Berries are natural items that can be used inside or outside of battle. The majority of Berries are 
              best used when held by Pokemon. Some berries can only be used outside of 
              battle to adjust the stats of Pokemon. Once a Pokemon uses the Berry, it will be gone forever.</p><br></br>
           
            <Link className="p-2 text-dark" to="/machines/page/0">
              Machines
              </Link>
              <p>A slot machine (Japanese: スロット slots) is a standard game at Game Corners in the Pokémon games, having appeared in every generation up until Generation IV.
              </p>
              <p>
              Slot symbols and their payouts tend to vary between generations, but the highest single payout is for three same-color sevens. In the first three generations, 
              the maximum payout is 300 coins; in Generation IV, the payout is a progressive jackpot starting at 100 coins.</p>
              <br></br>
            <Link className="p-2 text-dark" to="/pokemon/page/0">
              Pokemon
            </Link>
              <p>
              Pokémon are creatures of all shapes and sizes who live in the wild or alongside humans. For the most part, Pokémon do not speak except to utter their names. Pokémon 
              are raised and commanded by their owners (called “Trainers”). During their adventures, Pokémon grow and become more experienced and even, on occasion, evolve into stronger Pokémon.
              There are currently more than 700 creatures that inhabit the Pokémon universe.
              </p>
            <br></br>
          </nav>
        </div>
    )
  }
}
