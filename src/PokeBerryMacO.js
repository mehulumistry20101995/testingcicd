import React, { Component } from "react";
import { Link } from "react-router-dom";
import { PageHeader } from 'react-bootstrap';

class pokeBerryMacO extends Component {
  render() {
      return (
            <div>
            <Link className="p-2 text-dark" to="/">
              Home
            </Link>
            <PageHeader>{this.props.list.list.result.length} results on this page</PageHeader>
            <ul>
              {this.props.list.list.result.map(stat => {

                let url = stat.url.replace("https://pokeapi.co/api/v2/","")
                let tempUrl = url.split("/");
                if(tempUrl[0] === "pokemon"){
                  tempUrl[0] = "pokemon"
                }
                else if(tempUrl[0] === "berry"){
                  tempUrl[0] = "berries"
                }
                else if(tempUrl[0] === "machine"){
                  tempUrl[0] = "machines"
                }
                tempUrl = "/" +tempUrl[0] + "/" +tempUrl[1];
                return (
                  <li>
                    <div><b>{stat.name}</b>
                    &nbsp; &nbsp;
                    <Link key={stat.name} to={`${tempUrl}`}>
                    {stat.url}
                    </Link>
                    </div>
                  </li>
                );
              })}
            </ul>
          </div>    
        );
 }
}

export default pokeBerryMacO;